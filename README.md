# ASTERISK painel para monitorar fila

Painel extremamente simples para monitorar fila no IPBX

Escrito em PHP, para utiliza-lo basta realizar upload no apache no diretorio /var/www/html subir a pasta completa.

Ficando o usuário responsável pela desenvolvimento e customização da pagina,preenchendo a mesma com as cores e o logo da empresa. 

No caso o número 600 na linha 3 deve ser alterado para o número da fila que deseja monitorar.

Aplicação interessante para gerentes de call center, pois torna possivel saber em tempo real se determinado ramal está em ligação.
